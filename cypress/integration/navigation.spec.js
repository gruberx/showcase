/// <reference types="cypress" />
context('Navigation', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000')
  })
  it('Visit and Reload', () => {
    // https://on.cypress.io/reload
    cy.reload()
    // reload the page without using the cache
    cy.reload(true)
  })
  it('Website visit', () => {
    // https://on.cypress.io/visit
    // Visit any sub-domain of your current domain
    // Pass options to the visit
    cy.visit('http://localhost:3000', {
      timeout: 50000, // increase total time for the visit to resolve
      onBeforeLoad (contentWindow) {
        // contentWindow is the remote page's window object
        expect(typeof contentWindow === 'object').to.be.true
      },
      onLoad (contentWindow) {
        // contentWindow is the remote page's window object
        expect(typeof contentWindow === 'object').to.be.true
      },
    })
    })
  it('Checking terminal default', () => {
      cy.get('.container').find(".term-bar").contains("About me @: ~gitlab.com/vilanify")
      cy.get('.container').find(".term-cont > :nth-child(1)").contains("Welcome to Gruber's README. Type 'help' in order to see the available commands.")
      cy.get('.container').find(".term-ps").contains("~root@user:#")
    })
  it('Checking help command', () => {
    cy.get('input').type("help{enter}")
    cy.get('.container').contains("Available Commands")
    cy.get('.container').contains("email --help | show my email")
    cy.get('.container').contains("link --help | --list | visit my professional and personal urls")
    cy.get('input').last().type("email help{enter}")
    cy.get(".container").contains("Usage: email gruber --show")
  })
  it('Checking show email', () => {
    cy.get('input').type("email gruber --show{enter}")
    cy.get('.container').contains("Professional contact: gruber@navarratech.com")
  })
})
