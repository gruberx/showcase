# Portfolio showcase

  A simple static generated website deployed using Gitlab CI and hosted by Gitlab pages.

## Summary

- [Development](#getting-started)
  - [Built With](#running-the-tests)

### Development

You'll need either npm or yarn, then run the following command to install all the dependencies locally

```sh
    yarn install
```

To test locally in a hot-realoding mode:

```sh
yarn dev 
```


## Built With

- Nuxt.js
  - Vuetify
  - Gitlab Pages
  - Gitlab CI/CD
